package com.enkeapple.common.domain;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;

import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "messages")
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MessageEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column
    String value;

    @CreatedDate
    LocalDateTime createdAt;

    @PrePersist
    void toCreate() {
        setCreatedAt(LocalDateTime.now());
    }
}
