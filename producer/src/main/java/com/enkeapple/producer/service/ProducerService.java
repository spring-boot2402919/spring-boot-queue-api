package com.enkeapple.producer.service;

import com.enkeapple.common.domain.MessageEntity;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class ProducerService {
    final RabbitTemplate rabbitTemplate;

    @Autowired
    public ProducerService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Scheduled(fixedRate = 60000)
    public void sendMessage() {
        try {
            MessageEntity message = MessageEntity.builder()
                    .value("New message - " + System.currentTimeMillis())
                    .build();

            rabbitTemplate.convertAndSend("Queue", message);
        } catch (Exception e) {
            e.printStackTrace();

            throw new RuntimeException("Failed to send message", e);
        }
    }
}
