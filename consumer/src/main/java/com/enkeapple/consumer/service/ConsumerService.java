package com.enkeapple.consumer.service;

import com.enkeapple.common.domain.MessageEntity;
import com.enkeapple.common.repository.MessageRepository;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConsumerService {
    final MessageRepository repository;

    @Autowired
    public ConsumerService(MessageRepository repository) {
        this.repository = repository;
    }

    @RabbitListener(queues = "Queue")
    public void consumeMessage(MessageEntity message) {
        if (message != null) {
            repository.save(message);
        }
    }
}
